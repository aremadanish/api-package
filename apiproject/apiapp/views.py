from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect				# django imports
#============IMPORTS REQUIRED FOR GMAIL API========================
from apiclient import discovery
from apiclient import errors
from httplib2 import Http
from oauth2client import file, client, tools
import base64
from bs4 import BeautifulSoup
import re
import dateutil.parser as parser
#==============================IMPORTS REQUIRED FOR TWITTER API===================
import twitter
#==========================IMPORTS FOR FAEBOOK==================
from itertools import islice
from fbchat import Client
from fbchat.models import *
#=======================IMPORTS FOR INSTAGRAM
import instaloader


#=========FOR GMAIL=========
# Creating a storage.JSON file with authentication details
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly' # we are using readonly, as we will be reading the messages only
store = file.Storage('C:\\Users\\aaa\\Desktop\\New folder\\storage.json')   
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('C:\\Users\\aaa\\Downloads\\credentials.json', SCOPES)	# reading credentials
    creds = tools.run_flow(flow, store)					# creating credentials 
GMAIL = discovery.build('gmail', 'v1', http=creds.authorize(Http()))		# instantiating  gmail

#==========FOR TWITTER==============
api = twitter.Api(consumer_key="WbKIyEKEaOx73sqlYoxZS07v8",
	                  consumer_secret="FYHrVrfOu0C4Mw162IyEFozsFDK8TM23xnWItGTbndWcRUQwce",
	                  access_token_key="4280169014-xWC3yA4ENdPrnltodv6vCgcTFTdJk6KicAVld5I",
	                  access_token_secret="DejaoL6SxIokP35sjLjnHgzaPAzvys7Ws8yPH3qxI3Jo4")		# making twitter object

#=================FOR INSTAGRAM================
L = instaloader.Instaloader() 							# instagram object

#===================FOR SKYPE======================
from skpy import Skype
sk = Skype('mohammad.danish@arema.co.in', 'aremadanish123') # connect to SKYPE 	ENTER USERNAME AND PASSWORD

#======================FOR TELEGRAM===============
from telethon import TelegramClient, events, sync
api_id = 927094
api_hash = '944bee32d2b09e6c75e4e5e2aa9ec56a'



def index(requests):							# function for homepage screen
	return render(requests,'index.html')

def gmailUnread(requests):			# to get unread gmail emails
	user_id =  'me'
	label_id_one = 'INBOX'
	label_id_two = 'UNREAD'

	# Getting all the unread messages from Inbox
	# labelIds can be changed accordingly
	unread_msgs = GMAIL.users().messages().list(userId='me',labelIds=[label_id_one, label_id_two]).execute()

	# We get a dictonary. Now reading values for the key 'messages'
	mssg_list = unread_msgs['messages']
	total_msg = ("Total unread messages in inbox: ", str(len(mssg_list)))
	return HttpResponse(total_msg)


def gmailSent(requests):		# function for getting sent emails
	user_id =  'me'
	label_id_one = 'SENT'

	# Getting all the unread messages from Inbox
	# labelIds can be changed accordingly
	sent_msgs = GMAIL.users().messages().list(userId='me',labelIds=label_id_one).execute()

	# We get a dictonary. Now reading values for the key 'messages'
	mssg_list = sent_msgs['messages']
	total_msg = ("Total sent messages are: ", str(len(mssg_list)))
	return HttpResponse(total_msg)

def gmailDraft(requests):			# function for getting draft emails
	user_id =  'me'
	label_id_one = 'DRAFT'

	# Getting all the unread messages from Inbox
	# labelIds can be changed accordingly
	draft_msgs = GMAIL.users().messages().list(userId='me',labelIds=label_id_one).execute()

	# We get a dictonary. Now reading values for the key 'messages'
	mssg_list = draft_msgs['messages']
	total_msg = ("Total messages in Draft are: ", str(len(mssg_list)))
	return HttpResponse(total_msg)

def twitterfriends(requests):					# function for getting twitter friends
	users = api.GetFriends()					
	friends = ([u.name for u in users])
	return HttpResponse(friends)

def tweet(requests):					# function for posting tweet on twitter
	if requests.method=="POST":
		twt = requests.POST.get('tweet')
	
	status = api.PostUpdate(twt)
	print(status.text)

	return HttpResponse("Post Successful")

def fblogin(requests):							# this function calls fb login page
	return render(requests,'fblogin.html')

def fbhome(requests):							# getting username and password from fblogin page and logging in facebook
												# this function redirects to fb home page
	global client
	if requests.method == 'POST':
		uname = requests.POST.get('uname')
		upass = requests.POST.get('upass')

	client = Client(uname,upass)
	
	return render(requests,'fbhome.html')

def fbsend(requests):							# function for getting all the friends of the logged in user
	
	users = client.fetchAllUsers()
	#friend = client.searchForUsers("Kshitij Jain")[0]
	#user = friend.uid
	#client.send(Message(text="Sent from python!"), thread_id=user)
	friends = [user.name for user in users]
#	return HttpResponse(user.name for user in users)
	params = {'friends':friends}
	return render(requests,'fbsend.html',params)

def sendMessage(requests):					# getting friend and mesage and sending message 
	if requests.method == 'POST':
		frnd = requests.POST.get('fname')
		msg = requests.POST.get('msg')
	friend = client.searchForUsers(frnd)[0]
	user = friend.uid
	client.send(Message(text=msg), thread_id=user)
	return HttpResponse('Message Sent Successfully')

def insta(requests):						# function for instagram page
	return render(requests,'instalogin.html')

def instalogin(requests):						# function for logging into instagram
	global profile
	if requests.method == 'POST':
		uname = requests.POST.get('uname')
		upass = requests.POST.get('upass')

	L.login(uname,upass)
	profile = instaloader.Profile.from_username(L.context, uname)
	return render(requests,'instahome.html')

def instafollower(requests):					# function to get instagram followers
	followers = []
	for followee in profile.get_followers():
		followers.append(followee.username)
	return HttpResponse("Total Followers are "+str(len(followers)))

def instafollowing(requests):						# function to get instagram followings
	following = []
	for followee in profile.get_followees():
		following.append(followee.username)
	return HttpResponse("Total Following is "+ str(len(following)))

def skypedata(requests):				# function to get skype profile information
	profile_info = sk.user 					# you
	contact_info = sk.contacts				# your contacts
	chats_info = sk.chats 					# your conversations

	return HttpResponse("Personal Info "+str(profile_info)+' '+str(contact_info)+' '+str(chats_info))

def linedata(requests):
	return HttpResponseRedirect('https://testapp30112019.herokuapp.com/')
"""	client = TelegramClient('session_name', api_id, api_hash)
	client.start()

	return HttpResponse("Logged in")
"""
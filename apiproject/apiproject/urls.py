"""apiproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from apiapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/',views.index,name='index'),
    path('gmailUnread/',views.gmailUnread,name='gmailUnread'),
    path('gmailSent/',views.gmailSent,name='gmailSent'),
    path('gmailDraft/',views.gmailDraft,name='gmailDraft'),
    path('twitterfriends/',views.twitterfriends,name='twitterfriends'),
    path('tweet/',views.tweet,name='tweet'),
    path('fblogin/',views.fblogin,name='fblogin'),
    path('fbhome/',views.fbhome,name='fbhome'),
    path('fbsend/',views.fbsend,name='fbsend'),
    path('sendMessage/',views.sendMessage,name='sendMessage'),
    path('insta/',views.insta,name='insta'),
    path('instalogin/',views.instalogin,name='instalogin'),
#    path('instahome/',views.instahome,name='instahome'),
    path('instafollower/',views.instafollower,name='instafollower'),
    path('instafollowing/',views.instafollowing,name='instafollowing'),
    path('skypedata/',views.skypedata,name='skypedata'),
    path('linedata/',views.linedata,name='linedata'),
]

